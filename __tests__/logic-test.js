"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-redux-api-generator:logic-test", () => {
  beforeAll(() => {
    return helpers
      .run(path.join(__dirname, "../generators/logic-test"))
      .withPrompts({ someAnswer: true });
  });

  it("creates files", () => {
    assert.file(["dummyfile.txt"]);
  });
});
