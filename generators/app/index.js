const Generator = require("yeoman-generator");
const eslint = require("gulp-eslint");
const textTools = require("./util/textTools.js");
const jscs = require("jscodeshift");

const { toSlugCase, toSnakeCase, toLowerSpaceCase } =
  textTools.camelCaseConversions;

class MyAppGenerator extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this.argument("apiName", {
      desc: "apiName",
      required: false,
      type: String,
    });

    this.argument("apiUrl", {
      desc: "apiUrl",
      required: false,
      type: String,
    });

    this.argument("apiMethod", {
      desc: "apiMethod",
      required: false,
      type: String,
    });

    this.registerTransformStream([
      eslint({
        fix: true,
        quiet: true,
        cwd: this.templatePath("../../app/templates"),
      }),
    ]);
  }

  async prompting() {
    if (this.options.apiName) {
      return;
    }

    const answers = await this.prompt([
      {
        type: "input",
        name: "apiName",
        message: "Your api name",
      },
      {
        type: "input",
        name: "apiUrl",
        message: "Your api url",
      },
      {
        type: "input",
        name: "apiMethod",
        message: "Your api method",
      },
    ]);

    this.options = {
      ...this.options,
      apiName: answers.apiName,
      apiMethod: answers.apiMethod.split(","),
    };
  }

  checkName() {
    const { apiName } = this.options;

    if (apiName.length < 3) {
      throw new Error(`apiName "${apiName}" is less than 3 characters long`);
    }
  }

  _writeFileFromTemplate(templatePath, destinationPath, context) {
    this.fs.copyTpl(
      this.templatePath(templatePath),
      this.destinationPath(destinationPath),
      context
    );
    // TestFile &&
    //   this.fs.copyTpl(
    //     this.templatePath(interpolateTestInFileName(templatePath)),
    //     this.destinationPath(interpolateTestInFileName(destinationPath)),
    //     context
    //   );
  }

  _astAddImport(tree, identifier) {
    let existingImport = false;
    let hasAnyImports = false;

    tree.find("ImportDeclaration").forEach((path) => {
      hasAnyImports = true;
      const importPath = path.get("source", "value").value;

      if (importPath === `./${identifier}.js`) {
        existingImport = true;
      }
    });

    if (existingImport) {
      throw new Error("import already exists");
    }

    const codeToAdd = `import ${identifier} from './${identifier}.js'`;

    if (hasAnyImports) {
      tree.find("ImportDeclaration").get().insertBefore(codeToAdd);
    } else {
      tree.get("program", "body").unshift(codeToAdd);
    }
  }

  _addReducerToStore() {
    const { apiName, apiMethod } = this.options;

    const rootReducerProgram = this.fs.read(
      this.destinationPath("src/sdk/reducers/index.js")
    );

    const tree = jscs(rootReducerProgram);
    const identifier = `${apiName}Reducer`;

    try {
      this._astAddImport(tree, identifier);
    } catch (_) {
      return;
    }

    const codeToAdd = `${apiName}${apiMethod}: ${identifier}`;
    const updatedTree = tree
      .findVariableDeclarators("apiState")
      .find("ObjectExpression")
      .forEach((path) => path.get("properties").unshift(codeToAdd));

    this.fs.write("src/sdk/reducers/index.js", updatedTree.toSource());
  }

  _addLogicToRootLogic() {
    const { apiName } = this.options;

    const rootLogicProgram = this.fs.read(
      this.destinationPath("src/sdk/logics/index.js")
    );

    const tree = jscs(rootLogicProgram);
    const identifier = `${apiName}Logics`;

    try {
      this._astAddImport(tree, identifier);
    } catch (_) {
      return;
    }

    const codeToAdd = `...${identifier}`;
    const updatedTree = tree
      .findVariableDeclarators("rootLogics")
      .find("ArrayExpression")
      .forEach((path) => path.get("elements").unshift(codeToAdd));

    this.fs.write("src/sdk/logics/index.js", updatedTree.toSource());
  }

  writing() {
    const { apiName, apiMethod, apiUrl } = this.options;
    const actionName = `${apiName}`;
    const context = {
      upperSnakeCased: toSnakeCase(actionName).toUpperCase(),
      actionName: actionName,
      lowerSpaceCase: toLowerSpaceCase(apiName),
      name: apiName,
      apiUrl,
      apiMethod,
      slugCased: toSlugCase(apiName),
    };
    console.info(
      `Starting generate - ${context.actionName} - ${context.apiUrl} - ${context.apiMethod}`
    );
    const _getApi = apiMethod === "get";
    const hasRootReducer = this.fs.exists(
      this.destinationPath("src/sdk/reducers/index.js")
    );

    !hasRootReducer &&
      this._writeFileFromTemplate(
        "rootReducerTemplate.ejs",
        "src/sdk/reducers/index.js",
        context
      );

    this._addReducerToStore();

    const actiontemplateName = _getApi
      ? "actionsTemplateGet.ejs"
      : "actionsTemplateParams.ejs";
    this._writeFileFromTemplate(
      actiontemplateName,
      `src/sdk/actions/${actionName}Actions.js`,
      context,
      true
    );

    this._writeFileFromTemplate(
      "reducerTemplateAsync.ejs",
      `src/sdk/reducers/${actionName}Reducer.js`,
      context,
      true
    );

    const logictemplateName = _getApi
      ? "logicsTemplateParams.ejs"
      : "logicsTemplate.ejs";
    this._writeFileFromTemplate(
      logictemplateName,
      `src/sdk/logics/${actionName}Logics.js`,
      context
    );

    const hasRootLogic = this.fs.exists(
      this.destinationPath("src/sdk/logics/index.js")
    );

    !hasRootLogic &&
      this._writeFileFromTemplate(
        "rootLogicTemplate.ejs",
        "src/sdk/logics/index.js",
        context
      );

    this._addLogicToRootLogic();

    console.info(`****** Done`);
  }
}

// Export the class as default
module.exports = MyAppGenerator;
